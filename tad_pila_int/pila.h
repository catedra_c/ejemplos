#ifndef PILA_H
#define PILA_H
#include "lista.h"
// typedef struct nodo *lista;

typedef lista pila;
int push(pila *p, int d);
int pop(pila *p, int *d);
pila crear();
void eliminar(pila *p);

#endif
