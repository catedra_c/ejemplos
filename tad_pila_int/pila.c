#include "pila.h"
int push(pila *p, int d){
    // Si agregar_lista() retorna algún código de error lo usamos, sino
    // retornamos siempre que salió todo bien.
    agregar_lista(p, d);
    return 1;
}
int pop(pila *p, int *d){
    // Lo ideal sería usar funciones de la lista para acceder a sus elementos
    // pero si la lista no lo implementa podemos acceder a su estructura
    // interna y hacer lo que sea necesario. Esto no es recomendable pero es
    // posible.
    lista aux = (*p);
    if (aux != NULL) {
        *d = aux->dato;
        (*p) = aux->sig;
        free(aux);
        return 1;
    }
    return 0;
}
pila crear(){
    // Si crear_lista() retorna una lista vacía, lo mismo vale para pila
    return crear_lista();
}
void eliminar(pila *p){
    // Implementar de forma que se libere toda la memoria usada por la pila
}
