#include "pila.h"

int main() {
    int ret;
    int dato;
    pila p = crear();
    for (int i = 0; i < 100; i++) {
        push(&p, i);
        if (!ret) return 1;
    }

    while (pop(&p, &dato)) {
        printf("%d\n", dato);
    }
    eliminar(&p);
}
