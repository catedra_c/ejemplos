Esta carpeta contiene una implementación de ejemplo de un TAD pila.

El desafío para les estudiantes es combinar el ejemplo de lista enlazada
de la teoría con este tad pila para tener una pila funcional.

Se incluyen pila.c y pila.h con la implementación e interfaz de la pila
respectivamente y main.c con un ejemplo de uso de la misma.

Para compilar (habiendo agregado anteriormente lista.c y lista.h al
directorio):

```
gcc -Wall -o main main.c pila.c lista.c
```
