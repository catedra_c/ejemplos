#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
 * Este programa espera 3 argumentos:
 *  - `-s` o `-r` indicando si debe sumar o restar.
 *  - 2 números con los cuales operará.
 *
 *  El programa realizará la operación pedida e imprimirá
 *  en pantalla el resultado, no se verifica si los números
 *  son válidos.
 */

// Ejemplos de uso:
// ./operaciones -s 23 3
// ./operaciones -r 3 2
int main(int argumentos, char *arreglo[]){
    int a, b;
    // Verificamos que estén los 3 argumentos (el argumento 0, es
    // el nombre del programa, así que en realidad debería haber
    // 4 argumentos).
    if (argumentos == 4){
        char operacion;
        // Verificamos la operación
        if (strcmp(arreglo[1], "-s") == 0){
            operacion = 's';
        } else if (strcmp(arreglo[1], "-r") == 0) {
            operacion = 'r';
        } else {
            // Si no es "-s" o "-r" mostramos un error
            puts("Error, operacion");
            exit(1);
        }
        a = atoi(arreglo[2]); // atoi no verifica errores
        b = atoi(arreglo[3]);
        if (operacion == 's'){
            printf("%d\n", a + b);
        } else {
            printf("%d\n", a - b);
        }
    } else {
        // Si la cantidad de argumentos no es la esperada
        // mostramos un error.
        puts("Error, faltan o sobran argumentos");
        exit(2);
    }
}
