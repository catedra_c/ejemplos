#ifndef FECHA_H
#define FECHA_H
// ifdef y define arriba para evitar la doble inclusión

// Getters para obtener datos sin romper el encapsulamiento
#define FECHA_GET_ANIO(f) ((f).anio)
#define FECHA_GET_MES(f) ((f).mes)
#define FECHA_GET_DIA(f) ((f).dia)

// Declaración del tipo de datos
struct fecha {
    int anio;
    unsigned char mes;
    unsigned char dia;
};
// Alias para el tipo de datos por conveniencia
typedef struct fecha fecha_t;

// Prototipos de las funciones:
// `fecha_crear` inicializa una fecha (dado un puntero a la fecha)
void fecha_crear(fecha_t *f,
                 int anio,
                 unsigned char mes,
                 unsigned char dia);

// compara fechas con el mismo comportamiento que `strcmp`
// esto es útil para poder usar funciones como `qsort`
int fecha_cmp(const fecha_t f1,
              const fecha_t f2);

#endif
