#include <stdio.h>
#include <stdlib.h>
#include "fecha.h"

// Imprime una fecha sin romper el encapsulamiento del "TAD"
void print_fecha(fecha_t f){
    printf("%u/%u/%d\n",
           FECHA_GET_DIA(f),
           FECHA_GET_MES(f),
           FECHA_GET_ANIO(f));
}

// Función de comparación como la que requiere `qsort`, utiliza
// internamente la función de comparación del "TAD" pero recibe
// punteros en lugar de `fecha_t`
int fcmp(const void *f1, const void *f2){
    const fecha_t *ff1 = f1;
    const fecha_t *ff2 = f2;
    return fecha_cmp(*ff1, *ff2);
}

int main(){
    // Carga 3 fechas en un array de fechas, lo ordena de menor
    // a mayor utilizando `qsort` y lo imprime.
    fecha_t fs[3];

    fecha_crear(&fs[0], 2018, 10, 2);
    fecha_crear(&fs[1], 2009, 1, 2);
    fecha_crear(&fs[2], 2010, 2, 2);
    qsort(fs, 3, sizeof(fecha_t),
          fcmp);
    for (int i=0; i < 3; i++){
        print_fecha(fs[i]);
    }
}
