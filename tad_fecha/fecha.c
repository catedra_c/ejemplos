#include "fecha.h"
void fecha_crear(fecha_t *f,
                 int anio,
                 unsigned char mes,
                 unsigned char dia){
    f->anio = anio;
    f->mes = mes;
    f->dia = dia;
}
int fecha_cmp(const fecha_t f1,
              const fecha_t f2){
    int anio_dif;
    int dif;
    anio_dif = f1.anio - f2.anio;
    if (anio_dif) return anio_dif;
    dif = f1.mes - f2.mes;
    if (dif) return dif;
    return f1.dia - f2.dia;
}
