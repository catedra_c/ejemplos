Hay varias formas de compilar este ejemplo, algunas son:

* Compilar todo por separado y luego enlazar (linkeditar) todo en un ejecutable:

```
$ gcc -Wall -c main.c
$ gcc -Wall -c suma.c
$ gcc -Wall -c resta.c
$ gcc -Wall -o main main.o suma.o resta.o
```

* Compilar todo junto:

```
$ gcc -Wall -o main main.c suma.c resta.c
```
