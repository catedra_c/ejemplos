/*
 * `a` y `b` son locales a la función `x()`, por lo tanto su
 * visibilidad es local a `x()`, es decir no pueden ser accedidas
 * por otras funciones.
 *
 * `b` es una variable automática, se crea al invocar `x()` y se
 * destruye cuando `x()` termina. Se aloca en el stack.
 *
 * `a` es una variable estática, se crea al ejecutar el programa
 * y se destruye cuando el programa termina.
 */
int x() {
    static int a = 0;   // Se inicializa una vez
    int b = 0;          // Se inizializa en cada invocación a `x()`
    a++;                // Este valor se conservará
    b++;                // Este valor se perderá al terminar `x()`
    printf("a = %d, b = %d\n", a, b);
}
int main(){
    x();
    x();
    x();
    x();
}
