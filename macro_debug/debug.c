#include <stdio.h>

#define MUL(a, b) ((a) * (b))

#define DEBUGV(valor, letra) \
    printf("%s %d %" letra " <- " #valor "\n",\
            __func__,\
            __LINE__,\
            valor)

int main(){
    int a = 42;
    DEBUGV(a, "d");

    printf("5 + 10 = %d\n", MUL(5, 10 + 3) / 2);
}
