#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
int main(){
    char *linea = NULL;
    size_t alocado;
    FILE *f = fopen("archivo.algo", "r");
    if (f == NULL){
        puts("Error al abrir");
        return 1;
    }
    int cars = getline(&linea, &alocado, f);
    fclose(f);
    if (cars < 0){
        puts("Error al leer la linea");
        return 2;
    }
    int dia, mes, anio;
    int conv;
    conv = sscanf(linea, "%d/%d/%d", &dia, &mes, &anio);
    if (conv != 3) {
        puts("Fecha invalida");
        free(linea);
        return 3;
    }

    printf("Lei %d caracteres, "
           "fecha %d/%d/%d\n",
           cars, dia, mes, anio);


    free(linea);
}
