#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
int main(){
    char *linea;
    size_t alocado;
    FILE *f = fopen("archivo.algo", "r");
    if (f == NULL){
        puts("Error al abrir");
        return 1;
    }
    int cars = getline(&linea, &alocado, f);
    fclose(f);
    if (cars < 0){
        puts("Error al leer la linea");
        return 2;
    }
    long nro;
    char *fin_nro;
    nro = strtol(linea, &fin_nro, 10);

    if (linea == fin_nro || errno != 0) {
        puts("No hay nro");
        free(linea);
        return 3;
    }
    printf("Lei %d caracteres, "
           "nro vale %ld, "
           "fin_nro %s\n",
           cars, nro, fin_nro);


    free(linea);
}
