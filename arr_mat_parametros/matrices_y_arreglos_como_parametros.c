#include <stdio.h>
// Los argumentos de la función nunca son de
// tipo arreglo.
//
// Si en un parametro formal de una función uso corchetes,
// no es un arreglo ni una matriz es un puntero, por
// lo tanto su `sizeof` va a ser el tamaño de un puntero.
//
// Los argumentos van siempre por copia.

/*
 * El valor entre los corchetes no significa nada, es
 * necesario pasar un argumento con el tamaño del
 * arreglo al que se desea acceder.
 */
int sumo_array(int array[5], int len){
    int suma = 0;
    printf("%zd\n", sizeof(array)); // tamaño de un puntero
                                    // 8 o 4 en sistemas de 64 o 32bits
    for (int i = 0; i < len; i++) {
        suma += array[i];
    }
    return suma;
}
/*
 * Declarar `sumo_array()` con los siguientes prototipos es equivalente,
 * todas son distintas formas de decir que `sumo_array()` recibe
 * un puntero a `int` y un `int`:
 *  - int sumo_array(int array[5], int len);
 *  - int sumo_array(int array[2000], int len);
 *  - int sumo_array(int array[], int len);
 *  - int sumo_array(int *array, int len);
 *  La longitud del array no se verifica y no significa nada.
 */

/*
 * En el caso de las matrices en cambio, la cantidad de columnas
 * es necesaria para calcular la posición en memoria de los elementos.
 * La cantidad de filas no se verifica ni significa nada, así que
 * precisaríamos pasarla como argumento.
 */
int sumo_matriz(int matriz[][2], int filas){
    int suma = 0;
    printf("%zd\n", sizeof(matriz)); // tamaño de un puntero
                                    // 8 o 4 en sistemas de 64 o 32bits
    for (int i = 0; i < filas; i++){
        for (int j = 0; j < 2; j++){
            suma += matriz[i][j];
        }
    }
    return suma;
}
/*
 * Declarar `sumo_matriz()` de las siguientes formas es equivalente,
 * todas son distintas formas de decir que `sumo_matriz()` recibe
 * un "puntero a arreglos de 2 `int`" y un `int`.
 *  - int sumo_matriz(int matriz[][2], int filas);
 *  - int sumo_matriz(int matriz[2][2], int filas);
 *  - int sumo_matriz(int matriz[2000][2], int filas);
 *  - int sumo_matriz(int (*matriz)[2], int filas);
 */

int main(){
    int mat[][2] = { {1, 2},
                     {3, 4} };
    int array[] = {1, 2, 3, 4, 5};
    printf("%zd\n", sizeof(mat)); // sizeof(int) * 4
    printf("%zd\n", sizeof(array)); // sizeof(int) * 5

    printf("%d\n", sumo_array(array, 5));
    printf("%d\n", sumo_matriz(mat, 2));
}
