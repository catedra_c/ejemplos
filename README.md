# Ejemplos del Seminario de Lenguajes - Opción C

Este repositorio contiene algunos ejemplos puntuales para distintos temas vistos en la materia Seminario de Lenguajes - Opción C, de la [Facultad de Informática](http://www.info.unlp.edu.ar) de la [Universidad Nacional de La Plata](http://unlp.edu.ar).

## Organización

Los ejemplos están organizados en directorios individuales, siendo los siguientes los temas que cada uno trata:

* `args`: manejo de argumentos de un programa en C.
* `args_fptr`: versión idéntica a `args`, que utiliza punteros a funciones para evitar repetición innecesaria de código.
* `includes`: ejemplo simple del uso de `include` y modularización (notar que
en este ejemplo falta evitar el problema de la doble inclusión).
* `macro_condicional`: uso de `#ifdef` para determinar el sistema operativo
en el que se compila el programa.
* `macro_concat`: ejemplo del operador de concatenación `##`.
* `macro_debug`: ejemplo de uso de macros para imprimir rápidamente información
útil para depurar un programa, este ejemplo usa stringification (`#`) y
concatenation (`##`).
* `macro_sideffects`: ejemplo de problemas en el uso de macro, cuando un
argumento tiene un "side effect" (efecto lateral)  es decir, cuando evaluar un
argumento
significa realizar una acción que cambia el estado del programa, en este
caso un preincremento que modifica una variable.
* `args_int`: manejo simple de argumentos con valores numéricos al programa.
* `arr_mat_parametros`: matrices y arreglos como parámetros de funciones.
* `realloc`: uso (ineficiente) de `realloc()` para alocar espacio para una
línea medida que es leída por teclado (ver `man getline`).
* `tad_fecha`: evolución del ejercicio 6 de la práctica 4 utilizando
modularización en distintos archivos.
* `variables_locales_static`: pequeña demostración de una variable automática
vs una variable estática.
* `conversion_cadenas_a_entero`: dos programas para que leen una línea desde
un archivo con `getline()` y procesan números enteros en la línea.


## ¿Cómo utilizar estos ejemplos?

Se puede analizar el código fuente incluido en cada directorio, y compilarlo mediante el comando `make` para obtener el binario resultante. Por ejemplo, para compilar el ejemplo `args` se deben realizar los siguientes pasos en una interfaz de línea de comandos (o consola):

```console
$ git clone https://gitlab.com/catedra_c/ejemplos.git ejemplos_c
$ cd ejemplos_c/args
$ make
```

Eso resultará en la generación de un ejecutable en el mismo directorio `args`.

En cada ejemplo incluimos un archivo `README` que lo describe para simplificar su interpretación.
