El operador sizeof retorna el tamaño en bytes de un tipo de datos o valor.

Por ejemplo:

* `sizeof(int)`: es el tamaño de un `int` para el compilador y arquitectura
que esté usando.
* `int a; sizeof a`: también es el tamaño de un `int`.
* `sizeof 5`: lo mismo.
* `sizeof 5L`: es el tamaño de un `long int`.
