#include <stdio.h>

int suma_pares(int n) {
    int suma = 0;
    for (int i = 2; i <= n * 2; i += 2) {
	suma += i;
    }
    return suma;
}

int main() {
    printf("%d", suma_pares(250));
}
