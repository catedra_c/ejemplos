#include <stdio.h>
#include <string.h>

const char pass[] = "abracadabra";
const char texto[] = "C is quirky, flawed, and an enormous success. - Dennis Ritchie";

int main(int argc, char *argv[]){
    if (argc == 2 && strcmp(pass, argv[1]) == 0) {
        puts(texto);
    } else {
        printf("%d\n", argc);
    }
}
