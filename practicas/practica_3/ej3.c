#include <stdio.h> // puts
#include <string.h> // strncpy
#include <stdlib.h> // NULL

/*
 * Estas funciones están implementadas de forma similar a libc, pensar y
 * probar:
 *  - ¿Qué pasa si una de las cadenas no está terminada en cero?
 *  - ¿Qué pasa si `dest` no tiene espacio suficiente en una invocación
 *  a `my_strcpy()` o  `my_strcat()`?
 *  - ¿Qué pasa si alguno de los argumentos no está inicializado?
 */

//char *strcat(char *dest, const char *src);
char *my_strcat(char *dest, const char *src){
    char *start = dest;
    while (*dest != '\0') dest++;
    while (*src != '\0'){
        *dest = *src;
        dest++;
        src++;
    }
    *dest = '\0';
    return start;
}

//int strcmp(const char *s1, const char *s2);
int my_strcmp(const char *s1, const char *s2){
    int dif;
    while ((dif = *s1 - *s2) == 0){
        if (*s1 == 0) return 0;
        s1++;
        s2++;
    }
    return dif;
}

//char *strcpy(char *dest, const char *src);
char *my_strcpy(char *dest, const char *src){
    char *start = dest;
    while ((*dest = *src)) {
        dest++;
        src++;
    }
    return start;
}


//size_t strlen(const char *s);
size_t my_strlen(const char *s){
    size_t len;
    for (len = 0; s[len] != '\0'; len++);
    return len;
}

//char *strstr(const char *haystack, const char *needle);
char *my_strstr(const char *pajar, const char *aguja){
    int longitud = strlen(aguja);
    for (int i = 0; pajar[i] != '\0'; i++){
        if (strncmp(&pajar[i], aguja, longitud) == 0){
            return (char *) &pajar[i];
        }
    }
    return NULL;
}

int main(){
    char mensaje[1024];
    char *ptr;
    my_strcpy(mensaje, "Hola ");
    my_strcat(mensaje, "mundo!");
    printf("%zd\n", my_strlen(mensaje));
    printf(
        "%s\n",
        (my_strcmp(mensaje, "Hola mundo!") == 0)?"Iguales":"Distintos"
    );
    ptr = my_strstr(mensaje, "mund");
    printf( "%s\n", (ptr != NULL)?ptr:"No encontrado");
}

