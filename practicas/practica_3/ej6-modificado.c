#include <string.h>
#include <stdio.h>
/* Versión modificada del ejercicio X, donde en lugar de
 * imprimir en pantalla guardamos el string invertido en
 * el espacio de memoria apuntado por `dest`.
 */
char *invertir(char *dest, char *src){
    int len = strlen(src);
    int di, si;
    for (di = 0, si = len - 1; di < len; di++, si--){
        dest[di] = src[si];
    }
    dest[di] = '\0';
    return dest;
}

int main(){
    char msj[1024];
    invertir(msj, "Hola"); // -> "aloH"
    puts(msj);
}
