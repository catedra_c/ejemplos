#ifndef ALUMNO_H
#define ALUMNO_H

#include <stdlib.h>
#include "fecha.h"
struct alumno {
    char nombre[1024];
    char apellido[1024];
    fecha_t nacimiento;
};

typedef struct alumno alumno_t;

#define ALUMNO_GET_NOMBRE(alumno) alumno.nombre
#define ALUMNO_GET_APELLIDO(alumno) alumno.apellido
#define ALUMNO_GET_NACIMIENTO(alumno) alumno.nacimiento

void alumno_init(alumno_t *, char *, char *, fecha_t);
alumno_t alumno_copy(alumno_t);
void alumno_delete(alumno_t);
int alumno_to_str(alumno_t, char *, size_t);
int alumno_cmp_nombre(const alumno_t *, const alumno_t *);
int alumno_cmp_apellido(const alumno_t *, const alumno_t *);
int alumno_cmp_nacimiento(const alumno_t *, const alumno_t *);

#endif
