Ejercicio 10
------------

Este ejemplo contiene 2 resoluciones posibles del ejercicio 10:

* alumno.h y alumno.c: implementan el nombre y apellido como arrays.
* alumno_malloc.h y alumno_malloc.c: implementan el nombre y apellido
como punteros, al inicializar un alumno se aloca memoria para almacenar
estos datos.

Ambas soluciones se prueban con el programa principal main.c
(notar el ifdef al principio).

Para compilar:

1. Versión con array:
    ```
    gcc -Wall -std=c11 -o alumnos fecha.c alumno.c main.c
    ```

2. Versión con malloc:
    ```
    gcc -Wall -std=c11 -DUSAR_MALLOC -o alumnos_malloc fecha.c alumno_malloc.c main.c
    ```

Usando el Makefile:
```
make
```

Pensar y consultar:

* ¿Es correcto hacer una asignación entre 2 variables de tipo `alumno_t` en ambos ejemplos?
* ¿Por qué se implementan `alumno_delete` y `alumno_copy` en ambas versiones, si en la primera no hacen falta?
