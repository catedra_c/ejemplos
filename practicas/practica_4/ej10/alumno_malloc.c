#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "alumno_malloc.h"
#include "fecha.h"

void alumno_init(alumno_t *alumno,
                 char *nombre,
                 char *apellido,
                 fecha_t nacimiento)
{
    alumno->nombre = malloc(strlen(nombre) + 1);
    alumno->apellido = malloc(strlen(apellido) + 1);
    strcpy(alumno->nombre, nombre);
    strcpy(alumno->apellido, apellido);
    alumno->nacimiento = nacimiento;
}

alumno_t alumno_copy(alumno_t orig) {
    alumno_t copia;
    copia.nombre = malloc(strlen(orig.nombre) + 1);
    copia.apellido = malloc(strlen(orig.apellido) + 1);
    strcpy(copia.nombre, orig.nombre);
    strcpy(copia.apellido, orig.apellido);
    copia.nacimiento = orig.nacimiento;
    return copia;
}

void alumno_delete(alumno_t a) {
    // Es necesario liberar la memoria alocada
    free(a.nombre);
    free(a.apellido);
}

int alumno_to_str(alumno_t alumno, char *buffer, size_t len)
{
    int written = snprintf(
        buffer, len, "%u/%u/%u %s %s",
        FECHA_GET_ANIO(alumno.nacimiento),
        FECHA_GET_MES(alumno.nacimiento),
        FECHA_GET_DIA(alumno.nacimiento),
        alumno.nombre,
        alumno.apellido
    );
    return (written + 1) <= len;
}

int alumno_cmp_nombre(const alumno_t *a, const alumno_t *b)
{
    return strcmp(a->nombre, b->nombre);
}

int alumno_cmp_apellido(const alumno_t *a, const alumno_t *b)
{
    return strcmp(a->apellido, b->apellido);
}

int alumno_cmp_nacimiento(const alumno_t *a, const alumno_t *b)
{
    return fecha_cmp(a->nacimiento, b->nacimiento);
}
