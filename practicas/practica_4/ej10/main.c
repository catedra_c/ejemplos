#include <stdio.h>
#include "fecha.h"

#ifdef USAR_MALLOC
#include "alumno_malloc.h"
#else
#include "alumno.h"
#endif


typedef int (*fcmp)(const void *, const void *);

void print_alumno(alumno_t a) {
    char buffer[1024];
    if (alumno_to_str(a, buffer, 1024)) {
        puts(buffer);
    }
    else {
        puts("Error, los datos no entran en buffer");
    }
}

int main() {
    alumno_t a[3];
    alumno_t nuevo;
    fecha_t f;

    // Creo 3 alumnos en un array
    fecha_crear(&f, 2001, 10, 1);
    alumno_init(&a[0], "Juana", "Perez", f);

    fecha_crear(&f, 1990, 10, 1);
    alumno_init(&a[1], "Juan", "Rodriguez", f);

    fecha_crear(&f, 1990, 11, 1);
    alumno_init(&a[2], "Ana", "Rodriguez", f);

    // Imprimo ordenados por nombre
    puts("Ordenados por nombre");
    qsort(a, 3, sizeof(alumno_t), (fcmp) alumno_cmp_nombre);
    for (int i = 0; i < 3; i++) print_alumno(a[i]);

    // Imprimo ordenados por apellido
    puts("\nOrdenados por apellido");
    qsort(a, 3, sizeof(alumno_t), (fcmp) alumno_cmp_apellido);
    for (int i = 0; i < 3; i++) print_alumno(a[i]);

    // Imprimo ordenados por fecha de nacimiento
    puts("\nOrdenados por nacimiento");
    qsort(a, 3, sizeof(alumno_t), (fcmp) alumno_cmp_nacimiento);
    for (int i = 0; i < 3; i++) print_alumno(a[i]);


    // Copio un alumno
    nuevo = alumno_copy(a[0]);
    puts("\nAlumno copiado:");
    print_alumno(nuevo);
    alumno_delete(nuevo); // libero memoria si corresponde

    // Elimino los alumnos liberando memoria si corresponde
    for (int i = 0; i < 3; i++) alumno_delete(a[i]);
}
