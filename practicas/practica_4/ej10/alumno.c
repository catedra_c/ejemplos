#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "alumno.h"
#include "fecha.h"

void alumno_init(alumno_t *alumno,
                 char *nombre,
                 char *apellido,
                 fecha_t nacimiento)
{
    strcpy(alumno->nombre, nombre);	
    strcpy(alumno->apellido, apellido);
    alumno->nacimiento = nacimiento;	
}

alumno_t alumno_copy(alumno_t orig) {
    // Versión larga:
    /*
    alumno_t copia;
    copia = orig;
    return copia;
    */
    // Versión corta equivalente:
    return orig;
}

void alumno_delete(alumno_t a) { /* No hace falta hacer nada */ }

int alumno_to_str(alumno_t alumno, char *buffer, size_t len)
{
    int written = snprintf(
        buffer, len, "%u/%u/%u %s %s",
        FECHA_GET_ANIO(alumno.nacimiento),
        FECHA_GET_MES(alumno.nacimiento),
        FECHA_GET_DIA(alumno.nacimiento),
        alumno.nombre,
        alumno.apellido
    );
    return (written + 1) <= len;
}

int alumno_cmp_nombre(const alumno_t *a, const alumno_t *b)
{
    return strcmp(a->nombre, b->nombre);
}

int alumno_cmp_apellido(const alumno_t *a, const alumno_t *b)
{
    return strcmp(a->apellido, b->apellido);
}

int alumno_cmp_nacimiento(const alumno_t *a, const alumno_t *b)
{
    return fecha_cmp(a->nacimiento, b->nacimiento);
}
