#include <stdio.h>
#include <stdlib.h>
char *leer_linea(){
    int letra;
    char *linea = NULL;
    int alocado = 0;
    while ((letra = getchar()) != EOF && letra != '\n') {
        alocado++;
        linea = realloc(linea, alocado);
        linea[alocado - 1] = letra;
    }
    alocado++;
    linea = realloc(linea, alocado);
    linea[alocado - 1] = 0;
    return linea;
}

int main(){
    char *texto = leer_linea();
    printf("Lei: ");
    puts(texto);
    free(texto);
}
