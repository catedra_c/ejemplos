#define CONCAT(a, b) a##_##b

int suma_int(int a, int b){
    return a + b;
}

int suma_double(double a, double b){
    return a + b;
}

int main(){
    int a = suma_int(2, 5);
    int b = CONCAT(suma, int)(2, 5);
}
